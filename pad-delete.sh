#! /bin/bash
set -eu

# Permanently delete an etherpad pad from the database.
# Usage: pad-delete.sh PAD-ID
# BUG: The path to the API key file depends on etherpad version.

pad_id="$1"; shift
apikey="$(cat /home/etherpad/deploy/etherpad-lite-1.8.14/APIKEY.txt)"
curl "http://localhost:9001/api/1/deletePad?apikey=${apikey}&padID=${pad_id}"
